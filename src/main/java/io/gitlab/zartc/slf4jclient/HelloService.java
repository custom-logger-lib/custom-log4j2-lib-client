package io.gitlab.zartc.slf4jclient;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloService {

    @GetMapping
    String sayHello(String name) {
        if (StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException("name must not bu null or empty");
        }

        return "Hello " + name;
    }
}

/* EOF */
