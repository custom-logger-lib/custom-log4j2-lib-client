package io.gitlab.zartc.slf4jclient;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.log4j.*;

/*
 examine classes
 LoggingSystemProperties
 org.springframework.boot.logging.log4j2.Log4J2LoggingSystem

 https://stackoverflow.com/questions/27507450/better-way-to-initialize-log4j2-two-programmatically
 https://stackoverflow.com/questions/21083834/load-log4j2-configuration-file-programmatically

*/

@Log4j2
@SpringBootApplication
public class ClientApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ThreadContext.put("tenantId", "12345");
        ThreadContext.put("traceId", "6789");
        ThreadContext.put("spanId", "6789-a");

        log.fatal("fatalMsg");
        log.error("errorMsg");
        log.warn("warnMsg");
        log.info("infoMsg");
        log.debug("debugMsg");
        log.trace("traceMsg");

        log.throwing(new IllegalArgumentException("testException"));
        log.catching(new IllegalArgumentException("testException"));
    }
}
